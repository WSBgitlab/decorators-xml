
import json
import xmltodict


def unparse(buffer: str, **kwargs) -> dict:
    return xmltodict.unparse(buffer, **kwargs)


def parse(buffer: dict, **kwargs) -> str:
    return xmltodict.parse(buffer, **kwargs)
