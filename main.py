from dicttoxml import dicttoxml

def parse_with_kargs(args_parser):
    def receive_function(func):
        def execute_function(*args):
            xml = dicttoxml(args, custom_root=args_parser,attr_type=False)
            print(xml)
            pass
        return execute_function
    return receive_function

def parser_without_args(func):
    def execute(dicionary):
        xml = dicttoxml(dicionary)

        print(xml)
    return execute

if __name__ == '__main__':
    print("Encoders with decorators use Django")

    dicionary = { 
        'GetEntryByCidResponse': {
            "Signature": "",
            "ResponseTime": "",
            "CorrelationId": "",
            "Cid": "",
            "Entry": {
                "Key": "",
                "KeyType": "",
                "Account": {
                    "Participant": "",
                    "Branch": "",
                    "AccountNumber": "",
                    "AccountType":"",
                    "OpeningDate": ""
                },
                "Owner": {
                    "Type": "",
                    "TaxIdNumber":"",
                    "Name":""
                },
                "CreationDate":"",
                "KeyOwnershipDate":"",
            },
            "RequestId":""
        }
    }

    """
        <GetEntryByCidResponse>
            <Signature></Signature>
            <ResponseTime>2020-01-10T10:00:00Z</ResponseTime>
            <CorrelationId>a9f13566e19f5ca51329479a5bae60c5</CorrelationId>
            <Cid>ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb</Cid>
            <Entry>
                <Key>11122233300</Key>
                <KeyType>CPF</KeyType>
                <Account>
                    <Participant>12345678</Participant>
                    <Branch>0001</Branch>
                    <AccountNumber>0007654321</AccountNumber>
                    <AccountType>CACC</AccountType>
                    <OpeningDate>2010-01-10T03:00:00Z</OpeningDate>
                </Account>
                <Owner>
                    <Type>NATURAL_PERSON</Type>
                    <TaxIdNumber>11122233300</TaxIdNumber>
                    <Name>João Silva</Name>
                </Owner>
                <CreationDate>2019-11-18T03:00:00Z</CreationDate>
                <KeyOwnershipDate>2019-11-18T03:00:00Z</KeyOwnershipDate>
            </Entry>
            <RequestId>a946d533-7f22-42a5-9a9b-e87cd55c0f4d</RequestId>
        </GetEntryByCidResponse>
    
    """
    
    
    @parser_without_args
    def serializer(self,dicionary):
        return dicionary

    @parse_with_kargs('RootDocument')
    def serializer_dict(self,dicionary):
        return dicionary
    
    serializer(dicionary)
    
    serializer_dict(dicionary)

